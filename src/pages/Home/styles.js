import styled from 'styled-components';
import background from '../../assets/images/background.svg';

export const Container = styled.div`
  height: auto;
  min-height: 100vh;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  background: url(${background});
  background-size: cover;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 220px;
  height: 80px;
  margin: 2rem 0rem 4rem 0rem;
  hr {
    width: 100px;
    background: #6fcf97;
    border-style: none;
    border-bottom: 4px solid #6fcf97;
    margin-left: 0;
    margin-top: 0;
  }
`;
