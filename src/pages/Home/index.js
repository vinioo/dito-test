import React, { useState, useEffect } from 'react';
import API from '../../config/api';
import { formatEvent, groupProducts } from '../../util/event';

import Card from '../../components/Card';

import { Container, Content, Title } from './styles';

export default function Index() {
  const [items, setItem] = useState({ events: [] });

  let buyProductEvents = [];
  let buyEvents = [];

  useEffect(() => {
    getItems();
  }, []);

  const getItems = async () => {
    const result = await API.get('/events.json');

    setItem(result.data);
  };

  items.events.forEach(event =>
    event.event === 'comprou-produto' ? buyProductEvents.push(formatEvent(event)) : buyEvents.push(formatEvent(event))
  );

  buyEvents = groupProducts(buyProductEvents, buyEvents);
  return (
    <Container>
      <Content>
        <Title>
          <h1>Compras</h1>
          <hr />
        </Title>
        {items && buyEvents.map(item => <Card item={item} key={item.transaction_id} />)}
      </Content>
    </Container>
  );
}
