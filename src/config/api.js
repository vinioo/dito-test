import axios from 'axios';

const API = axios.create({
  baseURL: 'https://storage.googleapis.com/dito-questions'
});

export default API;
