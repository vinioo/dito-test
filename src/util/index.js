export function formatTimestamp(time) {
  const data = time
    .substr(0, 10)
    .split('-')
    .reverse()
    .join('/');
  const hours = time.substr(11, 5);
  return `${data} ${hours}`;
}
