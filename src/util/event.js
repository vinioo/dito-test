export const formatData = obj => {
  const formatedObject = {};

  obj.forEach(prop => {
    formatedObject[prop.key] = prop.value;
  });

  return formatedObject;
};

export const formatEvent = event => {
  return { ...event, ...formatData(event.custom_data) };
};

export const groupProducts = (buyProductEvents, buyEvents) => {
  return buyEvents.map(buy => {
    let buyFormatted = buy;
    buyFormatted.products = buyProductEvents.filter(product => product.transaction_id === buy.transaction_id);

    return buyFormatted;
  });
};
