import React from 'react';
import { ListItem } from './styles';

export default function index(props) {
  return (
    <ListItem>
      <div>
        <p>{props.name}</p>
      </div>
      <div>
        <p>R${props.price}</p>
      </div>
    </ListItem>
  );
}
