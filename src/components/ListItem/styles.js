import styled from 'styled-components';

export const ListItem = styled.div`
  display: flex;
  justify-content: space-between;
  box-shadow: 0px 0.5px 0px #e0e0e0;
  margin-bottom: 8px;
  div {
    width: 22%;
    text-align: left;
  }
`;
