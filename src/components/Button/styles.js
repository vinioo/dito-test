import styled from 'styled-components';

export const Button = styled.button`
  width: 35px;
  height: 35px;
  background: #39c881;
  border-radius: 50%;
  border: none;
  display: flex;
  justify-content: center;
  align-items: center;
  &:hover {
    background: #35a66e;
  }
`;

export const ButtonIcon = styled.img`
  width: 20px;
  height: 20px;
`;
