import React from 'react';

import checkIcon from '../../assets/icons/correct-symbol.svg';

import { Button, ButtonIcon } from './styles';

export default function index() {
  return (
    <>
      <Button>
        <ButtonIcon src={checkIcon} alt="check icon"></ButtonIcon>
      </Button>
    </>
  );
}
