import styled from 'styled-components';

export const Base = styled.div`
  box-sizing: border-box;
  background: rgba(130, 130, 130, 0.02);
  width: 470px;
  height: auto;
  min-height: 180px;
  position: relative;
  padding: 10px 35px;
  margin-bottom: 8px;
  &:after {
    content: '';
    position: absolute;
    top: 20%;
    left: 0;
    width: 5px;
    height: 80%;
    background: #f1f1f1;
  }
  button {
    position: absolute;
    left: -15px;
    top: 0;
  }
`;

export const CardContainer = styled.div`
  margin: 6px;
  .placeInfo {
    display: flex;
    align-items: baseline;
    margin-bottom: 4px;
  }
`;
export const Card = styled.div`
  width: 100%;
  height: auto;
  min-height: 120px;
  box-sizing: border-box;
  margin-bottom: 20px;
  background: #f1f1f1;
  border-radius: 0px 12px 12px 12px;
  padding: 12px 22px;
`;

export const CardHeader = styled.div`
  width: 100%;
  height: 42px;
  display: inline-flex;
  align-items: center;
  justify-content: space-between;
  border-radius: 0px 12px 0px 0px;
  box-shadow: 0px 0.5px 0px #e0e0e0;
  margin-bottom: 12px;
  h2 {
    font-weight: bold;
    color: #333333;
    align-self: center;
  }
  .details {
    text-align: right;
    p {
      align-self: center;
    }
  }
`;

export const CardItems = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  .title {
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin-bottom: 8px;
    div {
      width: 22%;
      text-align: left;
      p {
        font-weight: bold;
      }
    }
  }
`;

export const PlaceIcon = styled.img`
  width: 22px;
  height: 22px;
  margin-right: 6px;
`;

export const SmallIcon = styled.img`
  width: 10px;
  height: 10px;
  margin-right: 2px;
`;
