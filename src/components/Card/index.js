import React from 'react';

import { formatTimestamp } from '../../util';

import ListItem from '../ListItem';
import Button from '../Button';

import mapIcon from '../../assets/icons/maps-and-flags.svg';
import calendarIcon from '../../assets/icons/calendar.svg';
import bagIcon from '../../assets/icons/shopping-bag.svg';

import { Base, CardContainer, Card, CardHeader, CardItems, PlaceIcon, SmallIcon } from './styles';

export default function index(props) {
  const { item } = props;

  const timestamp = formatTimestamp(item.timestamp);
  return (
    <Base>
      <Button></Button>
      <CardContainer>
        <div className="placeInfo">
          <PlaceIcon src={mapIcon} alt="location icon"></PlaceIcon>
          <h2>{item.store_name}</h2>
        </div>
        <Card>
          <CardHeader>
            <h2>R$390,50</h2>
            <div className="details">
              <p>
                <SmallIcon src={calendarIcon} alt="calendar icon"></SmallIcon>
                {timestamp}
              </p>
              <p>
                <SmallIcon src={bagIcon}></SmallIcon>
                {item.products.length} {item.products.length === 1 ? 'item' : 'itens'}
              </p>
            </div>
          </CardHeader>
          <CardItems>
            <div className="title">
              <div>
                <p>Produtos</p>
              </div>
              <div>
                <p>Preço</p>
              </div>
            </div>
            {item.products.map(product => (
              <ListItem name={product.product_name} price={product.product_price} key={product.product_name}></ListItem>
            ))}
          </CardItems>
        </Card>
      </CardContainer>
    </Base>
  );
}
